
void mx_printchar(char c);

void mx_cube(int n) {
	if(n <= 1)
		return;

	int diagonals = n / 2;
	int spaces = diagonals + 1;
	
	//top back edge
	for(int i = 0; i < spaces; i++) {
		mx_printchar(' ');
	}
	
	mx_printchar('+');
	
	for(int i = 0; i < n * 2; i++) {
		mx_printchar('-');
	}
	
	mx_printchar('+');
	mx_printchar('\n');
	
	//top diagonals
	for(int i = 0; i < diagonals; i++) {
		spaces--;
		
		for(int s = 0; s < spaces; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('/');
		
		for(int s = 0; s < n * 2; s++) {
			mx_printchar(' ');
		}
			
		mx_printchar('/');
		
		for(int s = 0; s < diagonals - spaces; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('|');
		mx_printchar('\n');
	}
	
	//top front edge
	mx_printchar('+');
	
	for(int i = 0; i < n * 2; i++) {
		mx_printchar('-');
	}
	
	mx_printchar('+');
	
	for(int s = 0; s < diagonals; s++) {
		mx_printchar(' ');
	}
	
	mx_printchar('|');
	mx_printchar('\n');
	
	//vertical section
	for(int i = 0; i < n - diagonals - 1; i++) {
		mx_printchar('|');
		
		for(int s = 0; s < n * 2; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('|');
		
		for(int s = 0; s < diagonals; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('|');
		mx_printchar('\n');
	}
	
	//vertical section but with vertex at the end
	mx_printchar('|');
	
	for(int s = 0; s < n * 2; s++) {
		mx_printchar(' ');
	}
	
	mx_printchar('|');
	
	for(int s = 0; s < diagonals; s++) {
		mx_printchar(' ');
	}
	
	mx_printchar('+');
	mx_printchar('\n');
	
	//bottom diagonals
	for(int i = 0; i < diagonals; i++) {
		mx_printchar('|');
		
		for(int s = 0; s < n * 2; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('|');
		
		for(int s = 0; s < diagonals - i - 1; s++) {
			mx_printchar(' ');
		}
		
		mx_printchar('/');
		mx_printchar('\n');
	}
	//bottom edge
	mx_printchar('+');
	
	for(int i = 0; i < n * 2; i++) {
		mx_printchar('-');
	}
	
	mx_printchar('+');
	mx_printchar('\n');
}

