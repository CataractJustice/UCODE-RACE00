void mx_printchar(const char c);

void mx_pyramid(int n) {
	if (n <= 1 || n % 2 != 0) 
		return;
		
	for (int i = 0; i <= (n * 2 - 3) / 2; i++) {
		mx_printchar(' ');
	}
	
	mx_printchar('/');
	mx_printchar('\\');
	mx_printchar('\n');
						
	for (int i = 0; i < n / 2 - 1; i++) {
		for (int j = 0; j < (n * 2 - 3) / 2 - i; j++) {
			mx_printchar(' ');
		}
		
		mx_printchar('/');
		
		for (int j = 0; j < 2 * (i + 1) - 1; j++) {
			mx_printchar(' ');
		}
		
		mx_printchar('\\');
		
		for (int j = 0; j < (i + 2) - 1; j++) {
			mx_printchar(' ');
		}
		
		mx_printchar('\\');
		mx_printchar('\n');
	}
	
	for (int i = 0; i < n / 2; i++) {
		for (int j = 0; j < n / 2 - i - 1; j++) {
			mx_printchar(' ');
		}
		
		mx_printchar('/');
		
		for (int j = 0; j < 2 * (i + 1) + n - 3; j++) {
			mx_printchar(i == n / 2 - 1 ? '_' : ' ');
		}
		
		mx_printchar('\\');
		
		for (int j = i + 1; j < (n / 2); j++) {
			mx_printchar(' ');
		}
		
		mx_printchar('|');
		mx_printchar('\n');
	}
}

int main() {
	mx_pyramid(16);
}
